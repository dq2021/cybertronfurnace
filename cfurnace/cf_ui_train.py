import sys
import os
root_dir = os.getcwd()
sys.path.append(root_dir)
from cf_ui_train2 import cf_start_train

if len(sys.argv) > 1 and sys.argv[1] == 'cf_start_train':
    cf_start_train()
