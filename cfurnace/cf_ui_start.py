from cf_ui_api import app
from cf_ui_webscoket import app
from flask_socketio import SocketIO, emit
from geventwebsocket.handler import WebSocketHandler
import os
import sys
root_dir = os.getcwd()
sys.path.append(root_dir)

# app.run(host='127.0.0.1',port=5000)

from gevent import pywsgi
server = pywsgi.WSGIServer(('127.0.0.1', 5000), app, handler_class=WebSocketHandler)
print('server start')
server.serve_forever()


