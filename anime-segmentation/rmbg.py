import numpy as np

from inference import get_mask
from train import AnimeSegmentation, net_names


def rmbg_fn(model_path, img, img_size):
    mask = get_mask(model_cache[model_path], img, False, int(img_size))
    img = (mask * img + 255 * (1 - mask)).astype(np.uint8)
    mask = (mask * 255).astype(np.uint8)
    img = np.concatenate([img, mask], axis=2, dtype=np.uint8)
    mask = mask.repeat(3, axis=2)
    return mask, img

model_cache = {}

def load_model(path, net_name, img_size):
    if path in model_cache:
        return "model has already been loaded"
    model = AnimeSegmentation.try_load(
        net_name=net_name, img_size=int(img_size), ckpt_path=path, map_location="cpu"
    )
    model_cache[path] = model
    return "load model success"

def release_model():
    global model_cache
    for model in model_cache.values():
        del model
    model_cache = {}
    return "release model success"